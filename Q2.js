/*  
    Name: Troy Prujean
    Date: 29/03/2018
      ID: 9930285
*/

// Declaring variables
var userInput = 0;
var total = 0;
var average = 0;
var loopCount = 1;

// Outputting initial header to console before loop runs
console.log ("This application will find the average of ten numbers");
console.log ("-----------------------------------------------------");

// Do while loop that will run ten times (until the loopCount variable reaches 10)
do
{
    // Retrieving the user input and assigning it to the variable userInput (using a template string to show the number value to be that of the loopCount variable)
    userInput = Number (prompt(`Please enter number ${loopCount}`));
    // Outputting the result of the variable userInput inside the loop (using a template string to show the number value to be that of the loopCount variable)
    console.log (`Number ${loopCount}: ${userInput}`);
    // Calculating a running total inside the loop and assigning it to the variable total
    total += userInput;
    // Incrementing the variable loopCount +1 after each iteration
    loopCount ++;
}
// Once the loopCount value gets to 10 it will stop as the while condition will not let the loop run when loopCount reaches 11
while (loopCount != 11);

// Calculating the average buy dividing the total variable by 10, then assigning the result to the variable average
average = total / 10;

// Outputting the result in a template string
console.log (`The average of those 10 numbers is: ${average}`);


