/*  
    Name: Troy Prujean
    Date: 29/03/2018
      ID: 9930285
*/

// Declaring variables
var degreesCelcius = 0;
var degreesFahrenheit = 0;

// Requesting an input from the user for the degrees celcius and assigning the value to the variable degreesCelcius
degreesCelcius = Number (prompt("Please input a temperature in degrees Celcius to be converted:"));

// Converting the value of degreesCelcius to Fahrenheit and assigning the value to the variable degreesFahrenheit
degreesFahrenheit = (degreesCelcius * 18) / 10 + 32;

// Outputting the result to the browser console (using a template string for the conversion output)
console.log ("This application will convert degrees Celcius to degrees Fahrenheit:");
console.log ("--------------------------------------------------------------------");
console.log (`${degreesCelcius.toFixed(1)} degrees Celsius is equal to ${degreesFahrenheit.toFixed(1)} degrees Fahrenheit`);