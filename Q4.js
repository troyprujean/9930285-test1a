/*  
    Name: Troy Prujean
    Date: 29/03/2018
      ID: 9930285
*/

// Declaring variables
var userInput = "";
var output = "";

// Requesting the user to input a letter of the alphabet and assigning it to the variable userInput
userInput = prompt ("Enter a letter of the alphabet to determine if it is a vowel or a consonant");

// Switch case to determine if the userInput is a vowel by using all of the vowels, lower case and uppercase as "cases" - if the cases are met it will assign the variable output to "vowel"
// If the cases are not met it will default to assigning the variable output to "consonant"
switch (userInput)
{
    case "a":
    case "A":
    case "e":
    case "E":
    case "i":
    case "I":
    case "o":
    case "O":
    case "u":
    case "U":
        output = "vowel";
        break;

    default:
        output = "consonant";
        break;
}

// Outputting the result of the variable output in a template string with the initial user input
console.log ("This application determines if your input is a vowel or a consonant:");
console.log ("--------------------------------------------------------------------");
console.log (`${userInput} is a ${output}`);