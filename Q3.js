/*  
    Name: Troy Prujean
    Date: 29/03/2018
      ID: 9930285
*/

// Declaring variables
var customerID = "";
var customerName = "";
var customerUnits = 0;
var chargeUnit = 0;
var chargeTotal = 0;

// Outputting initial header
console.log ("This application will calculate the customer's Electricity Bill:");
console.log ("----------------------------------------------------------------");

// Requesting inputs from the user for the customers ID, name and amount of units used and storing them in the relevant declared variables and outputting the results to console straight away
customerID = prompt ("Please enter the customer's ID number:");
    console.log (`ID:                                      ${customerID}`);
customerName = prompt ("Please enter the customer's Name:");
    console.log (`Name:                                    ${customerName}`);
customerUnits = Number (prompt(`Please enter the number of units that ${customerName} has used:`));
    console.log (`Units:                                   ${customerUnits}\n\n`);

// IF statement to check the value of the customerUnits variable and then assign the chargeUnit variable to its appropriate value depending on the conditions met in the statement
if (customerUnits >= 600)
{
    chargeUnit = 2.00;
}
else if (customerUnits >= 400 && customerUnits < 600)
{
    chargeUnit = 1.80;
}
else if (customerUnits >= 200 && customerUnits < 400)
{
    chargeUnit = 1.50;
}
else if (customerUnits < 200)
{
    chargeUnit = 1.20;
}
else
{
    console.log (`An incorrect value was entered for the units that ${customerName} has used, please refresh and try again`);
}

// Calculating the total bill amount by multiplying the customerUnits by the value of chargeUnit (determined by the above IF statement) and assigning the result to the variable chargeTotal
chargeTotal = customerUnits * chargeUnit;

// Outputting the results of the calculation and formatting it to 2 decimal places
console.log ("Electricity Bill");
console.log ("----------------------------------------------------------------");
console.log (`The total owing @ $${chargeUnit.toFixed(2)} per unit is:     $${chargeTotal.toFixed(2)}`);